# UDP Port Knocking Client

This assignment consisted of two parts. First one was a [Server](https://gitlab.com/SlavMetal/udp-port-knocking-server), which should be started with a correct sequence of 'knocks' via UDP after which TCP port would become open for a client. Second one was a Client (this repository), which should be started with an address of a Server and a bunch of ports it should 'knock' the Server and (if successful) exchange some information via TCP.

## Build

```bash
git clone https://gitlab.com/SlavMetal/udp-port-knocking-client
cd udp-port-knocking-client
mvn package
```

## Run

General syntax:

```bash
java -jar /path/to/jar <server_address> <ports>
```

Fox example:

```bash
java -jar target/knock-client-1.0-SNAPSHOT-jar-with-dependencies.jar 127.0.0.1 4321 3547 4321
```

## License

Project is licensed under the GPLv3 (for terms and conditions see `LICENSE` file or visit [https://www.gnu.org/licenses/gpl.html](https://www.gnu.org/licenses/gpl.html)) unless where otherwise stated.